package http_client

import (
	"fmt"
	"net/http"
)

type Request struct {
	disableBaseUri bool
	client         *HttpClient
	request        *http.Request
	data           []byte
	err            error
}

func (req *Request) String() string {
	if req.err != nil {
		return fmt.Sprintf("error:%s", req.err)
	}

	data := req.GetBodyData()
	return fmt.Sprintf("body: %s, url: %s", string(data), req.request.URL.String())
}

func (req *Request) GetBodyData() []byte {
	return req.data
}

// 设置请求头
func (req *Request) SetHeader(key, val string) *Request {
	if req.IsError() {
		return req
	}
	req.request.Header.Set(key, val)
	return req
}

func (req *Request) AddHeader(key, val string) *Request {
	if req.IsError() {
		return req
	}
	req.request.Header.Add(key, val)
	return req
}

// 添加cookie
func (req *Request) AddCookie(cookie *Cookie) *Request {
	if req.IsError() {
		return req
	}
	req.request.AddCookie(cookie.cookie)
	return req
}

// 请求开始
func (req *Request) Do(printFn func(resp *Response, request *Request)) *Response {
	resp := &Response{}
	if req.IsError() {
		resp.err = req.GetError()
		return resp
	}
	resp.response, resp.err = req.client.client.Do(req.request)
	if printFn != nil {
		resp.GetBodyData()
		printFn(resp, req)
	}
	return resp
}

// 是否错误
func (req *Request) IsError() bool {
	return req.err != nil
}

// 获取错误
func (req *Request) GetError() error {
	return req.err
}
