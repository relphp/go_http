package http_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	errors "gitee.com/relphp/go_errors"
	"github.com/PuerkitoBio/goquery"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
)

type Response struct {
	response  *http.Response
	err       error
	data      []byte
	notClosed bool
	mutex     *sync.Mutex
}

func (resp *Response) String() string {
	if resp.err != nil {
		return fmt.Sprintf("error: %s", resp.err)
	}
	resp.GetBodyData()
	if resp.IsError() {
		return fmt.Sprintf("error: %s,body: %s", resp.err, string(resp.data))
	} else {
		return fmt.Sprintf("body: %s", string(resp.data))

	}
}

func (resp *Response) GetBodyData() []byte {
	defer resp.Close()
	if resp.IsError() {
		return nil
	}

	if resp.data != nil {
		return resp.data
	}

	resp.data, resp.err = ioutil.ReadAll(resp.response.Body)
	return resp.data
}

// 获取body
func (resp *Response) Body(data io.Writer) *Response {
	bodyData := resp.GetBodyData()
	if resp.IsError() {
		return resp
	}
	_, resp.err = io.Copy(data, bytes.NewReader(bodyData))
	return resp
}

// 获取body json
func (resp *Response) Json(data interface{}) *Response {
	if resp.IsError() {
		return resp
	}

	rawData := resp.GetBodyData()
	if resp.IsError() {
		return resp
	}
	resp.err = json.Unmarshal(rawData, data)
	return resp
}

// 关闭
func (resp *Response) Close() *Response {
	if resp.response != nil && resp.response.Body != nil {
		if resp.notClosed {
			resp.mutex.Lock()
			defer resp.mutex.Unlock()
			if resp.notClosed {
				_ = resp.response.Body.Close()
				resp.notClosed = false
			}
		}
	}

	return resp
}

// 检查
var STATUS_CODE_ERR = errors.New("check status code not pass")

func (resp *Response) Check(statusCode int) *Response {
	if resp.IsError() {
		return resp
	}

	if resp.response.StatusCode != statusCode {
		resp.err = STATUS_CODE_ERR
	}
	return resp
}

// 获取cookie
func (resp *Response) Cookies() []*http.Cookie {
	return resp.response.Cookies()
}

//获取响应头
func (resp *Response) Header(key string) (error, string) {
	if resp.IsError() {
		return resp.GetError(), ""
	}

	return nil, resp.response.Header.Get(key)
}

// 获取状态值
func (resp *Response) StatusCode() (error, int) {
	if resp.IsError() {
		return resp.GetError(), 0
	}
	return nil, resp.response.StatusCode
}

// 是否是错误
func (resp *Response) IsError() bool {
	return resp.err != nil
}

func (resp *Response) GetError() error {
	return resp.err
}

type Document struct {
	*goquery.Document
	err error
}

// 获取body html
func (resp *Response) Html() *Document {
	d := &Document{}
	if resp.IsError() {
		d.err = resp.GetError()
		return d
	}

	rawData := resp.GetBodyData()
	if resp.IsError() {
		d.err = resp.GetError()
		return d
	}

	d.Document, d.err = goquery.NewDocumentFromReader(bytes.NewReader(rawData))
	return d
}

// 是否是错误
func (doc *Document) IsError() bool {
	return doc.err != nil
}

func (doc *Document) GetError() error {
	return doc.err
}
