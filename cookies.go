package http_client

import (
	"net/http"
	"time"
)

type Cookie struct {
	cookie *http.Cookie
}

func NewCookie(name, value string) *Cookie {
	return &Cookie{&http.Cookie{Name: name, Value: value}}
}

func (cookie *Cookie) SetDomain(domain string) *Cookie {
	cookie.cookie.Domain = domain
	return cookie
}

func (cookie *Cookie) SetExpires(expire time.Time) *Cookie {
	cookie.cookie.Expires = expire
	return cookie
}

func (cookie *Cookie) SetHttpOnly(httpOnly bool) *Cookie {
	cookie.cookie.HttpOnly = httpOnly
	return cookie
}

func (cookie *Cookie) SetSecure(secure bool) *Cookie {
	cookie.cookie.Secure = secure
	return cookie
}
