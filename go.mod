module gitee.com/relphp/go_http

go 1.15

require (
	gitee.com/relphp/go_errors v0.0.0-20210604080149-aa5006dc6282
	github.com/PuerkitoBio/goquery v1.6.1
)
