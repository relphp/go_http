package http_client

import (
	"bytes"
	"encoding/json"
	"fmt"
	errors "gitee.com/relphp/go_errors"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
)

type HttpClient struct {
	client *http.Client
	err    error
	log    Logger
}

type Logger interface {
	Errorf(format string, option ...interface{})
	Infof(format string, option ...interface{})
}

// 默认日志不打印任何数据
type defaultLogger struct{}

func (l *defaultLogger) Errorf(format string, option ...interface{}) {
}

func (l *defaultLogger) Infof(format string, option ...interface{}) {
}

func NewClient(options ...Option) *HttpClient {
	client := &HttpClient{
		client: &http.Client{},
		log:    &defaultLogger{},
	}
	if len(options) > 0 {
		for _, op := range options {
			op(client)
			if client.IsError() {
				return client
			}
		}
	}
	return client
}

func (client *HttpClient) IsError() bool {
	return client.err != nil
}

func (client *HttpClient) GetError() error {
	return client.err
}

// get请求
func (client *HttpClient) Get(url string, params *url.Values) *Request {
	if client.IsError() {
		return &Request{err: client.GetError()}
	}
	if params != nil {
		url = fmt.Sprintf("%s?%s", url, params.Encode())
	}
	request := client.Request(http.MethodGet, url, nil)
	return request
}

// post请求
func (client *HttpClient) Post(url string, body io.Reader) *Request {
	if client.IsError() {
		return &Request{err: client.GetError()}
	}
	request := client.Request(http.MethodPost, url, body)
	return request
}

// post form 请求
func (client *HttpClient) PostForm(url string, params *url.Values) *Request {
	if client.IsError() {
		return &Request{err: client.GetError()}
	}
	if params == nil {
		return client.Post(url, nil).
			SetHeader("Content-Type", "application/x-www-form-urlencoded")
	} else {
		encodeBt := []byte(params.Encode())
		req := client.Post(url, bytes.NewReader(encodeBt)).
			SetHeader("Content-Type", "application/x-www-form-urlencoded")
		req.data = encodeBt
		return req
	}
}

// post json 请求
func (client *HttpClient) PostJson(url string, params interface{}) *Request {
	if client.IsError() {
		return &Request{err: client.GetError()}
	}
	var bt []byte
	var err error
	if params != nil {
		bt, err = json.Marshal(params)
	}

	var req *Request
	if err == nil {
		req = client.Post(url, bytes.NewReader(bt))
	} else {
		client.log.Errorf("PostJson Marshal failed; error:%s", err)
		req = &Request{err: errors.Here(err)}
	}
	req.data = bt
	return req
}

// file请求
func (client *HttpClient) FormFile(url, fieldName, fileName string, body io.Reader) *Request {
	r := &Request{}
	if client.IsError() {
		r.err = client.GetError()
		return r
	}
	buf := &bytes.Buffer{}
	writer := multipart.NewWriter(buf)
	form, err := writer.CreateFormFile(fieldName, fileName)
	if err != nil {
		r.err = err
		return r
	}

	n, err := io.Copy(form, body)
	if err != nil {
		r.err = err
		return r
	}

	if n <= 0 {
		r.err = errors.New("file read len 0")
		return r
	}

	err = writer.Close()
	if err != nil {
		r.err = err
		return r
	}

	r = client.Post(url, buf)
	r.data = buf.Bytes()
	r.SetHeader("Content-Type", writer.FormDataContentType())
	return r
}

//实例化请求
func (client *HttpClient) Request(method string, url string, body io.Reader) (req *Request) {
	if client.IsError() {
		return &Request{err: client.GetError()}
	}

	req = &Request{client: client}
	req.request, req.err = http.NewRequest(method, url, body)
	client.log.Infof("request method: %s, url: %s, err:%v", method, url, req.err)
	return req
}
