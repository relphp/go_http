package http_client

import (
	"fmt"
	"strings"
)

type BaseURL string

func NewBaseURL(baseUrl string) *BaseURL {
	tmp := BaseURL(strings.TrimRight(baseUrl, "/"))
	return &tmp
}

func (base *BaseURL) String() string {
	return string(*base)
}

func (base *BaseURL) Url(uri string) string {
	return fmt.Sprintf("%s/%s", *base, strings.TrimLeft(uri, "/"))
}
