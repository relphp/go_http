package http_client

import (
	"fmt"
	"testing"
)

func TestBaseURL_String(t *testing.T) {
	testUrl := "http://www.baidu.com"
	url := NewBaseURL(testUrl)
	tmp := fmt.Sprint(url)
	if tmp != testUrl {
		t.Error("failed")
	}
}

func TestBaseURL_Url(t *testing.T) {
	testUrl := "http://www.baidu.com"
	url := NewBaseURL(testUrl)
	if url.Url("a/b") != fmt.Sprintf("%s/%s", testUrl, "a/b") {
		t.Error("failed")
	}

	if url.Url("//a/b") != fmt.Sprintf("%s/%s", testUrl, "a/b") {
		t.Error("failed")
	}
}
