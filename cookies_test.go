package http_client

import (
	"testing"
	"time"
)

func TestNewCookie(t *testing.T) {
	testTime := time.Unix(3600*24, 0).Add(10 * time.Second)
	cookie := NewCookie("cookie", "fuck").
		SetDomain("www.baidu.com").
		SetExpires(testTime).
		SetSecure(true).
		SetHttpOnly(true)

	if cookie.cookie.Secure != true || cookie.cookie.HttpOnly != true || !cookie.cookie.Expires.Equal(testTime) {
		t.Error("failed")
	}

	if cookie.cookie.Domain != "www.baidu.com" || cookie.cookie.Name != "cookie" || cookie.cookie.Value != "fuck" {
		t.Error("failed")
	}
}
