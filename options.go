package http_client

import (
	"crypto/tls"
	"crypto/x509"
	errors "gitee.com/relphp/go_errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Option = func(client *HttpClient)

// 跳过证书域名验证
func WithSkipVerify() Option {
	return func(client *HttpClient) {
		if client.client.Transport == nil {
			client.client.Transport = http.DefaultTransport
		}

		transport, ok := client.client.Transport.(*http.Transport)
		if !ok {
			client.log.Errorf("transport  isn't http.Transport")
			client.err = errors.New("not support")
			return
		}

		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{
				InsecureSkipVerify: true,
			}
		} else {
			transport.TLSClientConfig.InsecureSkipVerify = true
		}
	}
}

// x509证书
func WithX509Cert(pemFile, keyFile string) Option {
	return func(client *HttpClient) {
		if client.client.Transport != nil {
			client.client.Transport = http.DefaultTransport
		}

		transport, ok := client.client.Transport.(*http.Transport)
		if !ok {
			client.log.Errorf("transport  isn't http.Transport")
			client.err = errors.New("not support")
			return
		}

		if transport.TLSClientConfig == nil {
			transport.TLSClientConfig = &tls.Config{}
		}

		cert, err := tls.LoadX509KeyPair(pemFile, keyFile)
		if err != nil {
			client.log.Errorf("LoadX509KeyPair  failed, err: %s", err)
			client.err = errors.Here(err)
			return
		}

		certBytes, err := ioutil.ReadFile(pemFile)
		if err != nil {
			client.log.Errorf("ReadFile  failed, err: %s", err)
			client.err = errors.Here(err)
			return
		}

		clientCertPool := x509.NewCertPool()
		ok = clientCertPool.AppendCertsFromPEM(certBytes)
		if !ok {
			client.log.Errorf("AppendCertsFromPEM  failed, err: %s", err)
			client.err = errors.New("append certs from pem failed")
			return
		}

		transport.TLSClientConfig.RootCAs = clientCertPool
		transport.TLSClientConfig.Certificates = []tls.Certificate{cert}
		transport.TLSClientConfig.InsecureSkipVerify = true
	}
}

// 代理
func WithProxy(proxy func(*http.Request) (*url.URL, error)) Option {
	return func(client *HttpClient) {
		if client.client.Transport != nil {
			client.client.Transport = http.DefaultTransport
		}

		transport, ok := client.client.Transport.(*http.Transport)
		if !ok {
			client.log.Errorf("transport  isn't http.Transport")
			client.err = errors.New("not support")
			return
		}

		transport.Proxy = proxy
	}
}

// 重定向规则
func WithCheckRedirect(cb func(req *http.Request, via []*http.Request) error) Option {
	return func(client *HttpClient) {
		client.client.CheckRedirect = cb
	}
}

// 日志

func WithLogger(logger Logger) Option {
	return func(client *HttpClient) {
		client.log = logger
	}
}
